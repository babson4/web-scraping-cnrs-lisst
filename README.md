# web-scraping-CNRS-LISST

Recuperation d'informations sur Facebook et Twitter par rapport à un corpus de page dans le cadre d'un stage au LISST

# Facebook	
### Prerequis
- [Java](https://www.java.com/fr/download/) (developper sous javaSE1.8.0_201)
- [geckodriver](https://github.com/mozilla/geckodriver/releases/tag/v0.24.0) le mettre à la racine du projet: `web-scraping-CNRS-LISST/`
- [Firefox](https://www.mozilla.org/fr/firefox/new/) >= 67.0.2 (voir geckodriver)


Utilisation d'un scraper en java:

``` 
Dans le fichier input/pagesInfo.txt mettre les informations des pages à récolter.
prenons pour exemple la page https://www.facebook.com/FacebookFrance/
FacebookPageName = FacebookFrance
FacebookPageID = 179106818789009
Pout trouver l'ID de la page: clic droit sur l'image de la page pui copier l'adresse du lien. Puis coller et garder le nombre après le permier slash (hors https://)
```
![https://puu.sh/DHxGT/895dd2cade.png](https://puu.sh/DHxMj/47506e91ba.png)
![https://puu.sh/DHxGT/895dd2cade.png](https://puu.sh/DHycn/0bc4885cf1.png)

### Reseau de page aimées

A la racine: executer la commande `java -jar RunScraper.jar reseau NomDuCorpusTraiter IdentifiantConexionFacebook MotDePasse`

### Collecte des posts

A la racine: executer la commande `java -jar RunScraper.jar contenu NomDuCorpusTraiter IdentifiantConexionFacebook MotDePasse`

### Resultats

Tous les résulats se trouve dans le fichier `output/NomDuCorpusTraiter/Facebook`

### Iramuteq

dans le répertoire `src/script`:
exectuer `python date_switch.py (fichier.txt)` puis `python remove_http_facebook.py (date_switch_out.txt)` pour retirer les URL dans le corpus (optionel)


# Twitter
Utilisation de twint

- python >= 3.6
- [twint](https://github.com/twintproject/twint)
- [twython](https://twython.readthedocs.io/en/latest/)
- [pandas](https://pandas.pydata.org/pandas-docs/stable/install.html)

```
 Dans le fichier input/keyword.txt mettre mot clés à récolter.
 /!\ #twitter != twitter

 Dans le fichier input/auth.txt mettre la/les clée(s) API twitter 
``` 
Pour plus d'information sur l'API twitter voir [ici](https://developer.twitter.com/)

### Collecte des tweets

executer le fichier `ScraperTwint` situé dans le fichier `src/twitter` (.ps1 pour windows, .sh pour mac et linux)


### Resultats

Tous les résulats se trouve dans le fichier `output/NomDuCorpusTraiter/Twitter`

### Iramuteq


Utiliser les scripts dans l'ordre suivant: 
  - `concatCSV.py` --> prend les fichier avec le meme nom et les concatène 
    (par exemple #test.csv et test.csv sont concaténé dans test_combined.csv
  - `APIGetBiography NomDuCorpus` pour récolter les biographie des personnes collecté. 
  - `createIramuteqCorpus (fichier bio.txt du script précédent) NomDuCorpus`.
