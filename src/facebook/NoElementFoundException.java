package facebook;

public class NoElementFoundException extends Exception {

	private static final long serialVersionUID = -6851608182993900566L;

	public NoElementFoundException(String message) {
		super(message);
	}
}