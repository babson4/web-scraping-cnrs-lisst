package facebook;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class SeleniumScraper {
	private FirefoxOptions options;
	private WebDriver driver;
	private static final String OS = System.getProperty("os.name").toLowerCase();

	public SeleniumScraper() {

		if (this.isWindows()) {
			System.setProperty("webwebdriver.gecko.driver", "../web-scraping-cnrs-lisst/geckodriver.exe");
		} else if (this.isUnix() || this.isMac()) {
			System.setProperty("webwebdriver.gecko.driver", "../web-scraping-cnrs-lisst/geckodriver");
		} else {
			System.out.println("OS non pris en charge par l'application");
		}

		// Initialisation du driver
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "../web-scraping-cnrs-lisst/logs.txt");

		this.setOptions(new FirefoxOptions());

		// retirer l'affichage du navigateur
		this.getOptions().setHeadless(true);
		// affichage des images
		this.getOptions().addPreference("permissions.default.image", 2);
		// desactiver notifications
		this.getOptions().addPreference("dom.webnotifications.enabled", false);

		// LE COMPTE FACEBOOK DOIT ETRE EN FRANCAIS

		this.setDriver(new FirefoxDriver(this.getOptions()));

		this.getDriver().manage().window().maximize();

	}

	public void facebookConnection(String user, String pass) throws WebScrapError {
		// connexion a facebook
		this.getDriver().get("http://www.facebook.com/login");
		this.getDriver().findElement(By.id("email")).sendKeys(user);
		this.getDriver().findElement(By.id("pass")).sendKeys(pass);
		this.getDriver().findElement(By.id("loginbutton")).click();

		if (this.getDriver().getCurrentUrl().toString().contains("login_attempt"))
			throw new WebScrapError("Impossible de se connecter a Facebook: identifiants incorrect");

	}

	public void createDirectories(String corpus) {
		File dir = new File("../web-scraping-cnrs-lisst/output/" + corpus + "/Facebook/");
		// cree les repertoire dans le fichier output
		dir.mkdirs();
		dir = new File("../web-scraping-cnrs-lisst/output/" + corpus + "/Twitter/");
		dir.mkdirs();
	}

	public FirefoxOptions getOptions() {
		return this.options;
	}

	public void setOptions(FirefoxOptions options) {
		this.options = options;
	}

	public WebDriver getDriver() {
		return this.driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public boolean isWindows() {
		return OS.contains("win");
	}

	public boolean isMac() {
		return OS.contains("mac");
	}

	public boolean isUnix() {
		return OS.contains("nix") || OS.contains("nux") || OS.contains("aix");
	}

}
