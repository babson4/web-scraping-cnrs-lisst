package facebook;

import java.util.Arrays;

public class RunScraper {

	/**
	 *
	 * @param args: reseau/contenu NomDuCorpus IdentifiantFacebook PassFacebook
	 * @throws WebScrapError
	 */

	public static void main(String[] args) throws WebScrapError {
		if (args.length != 4)
			throw new WebScrapError(
					"Nombre d'argument non valide\n Merci de spécifier en arguments: reseaux/contenu NomDuCorpus IdentifiantFacebook PassFacebook");

		if (args[0].toLowerCase().equals("reseau") || args[0].toLowerCase().equals("r")) {
			// on retire la première valeur de args.
			FannedPages.main(Arrays.copyOfRange(args, 1, args.length));
		} else if (args[0].toLowerCase().equals("contenu") || args[0].toLowerCase().equals("c")) {
			ContentPageScraper.main(Arrays.copyOfRange(args, 1, args.length));
		} else {
			System.err.println("Merci de spécifier l'action que vous souhaitez: reseau (r), contenu (c)");
		}
	}
}
