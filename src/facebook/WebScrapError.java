package facebook;

public class WebScrapError extends Exception {

	private static final long serialVersionUID = 4858061163865288086L;

	public WebScrapError(String message) {
		super(message);
	}
}
