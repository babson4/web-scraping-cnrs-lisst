package facebook;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class FannedPages {

	/**
	 * @since 2019-05-20
	 * @author Bastien Sibout contact:<a> bastiensib@hotmail.fr</a>
	 * @param corpus: nom du corpus traiter (corespond au nom de dossier de sortie)
	 * @param id:     identifiant de connexion facebook
	 * @param pass:   mot de passe du compte facebook
	 * @throws WebScrapError
	 */

	public static void main(String[] args) throws WebScrapError {
		if (args.length != 3)
			throw new WebScrapError(
					"Nombre d'argument non valide\n Merci de spécifier en arguments: NomDuCorpus IdentifiantCompteFacebook MotDePassFacebook");

		SeleniumScraper scraper = new SeleniumScraper();

//		creation des repertoire
		scraper.createDirectories(args[0]);
		System.out.println("created");

//		Printer pour le fichier de résultat
		BufferedWriter writer = null;
		try {

			String corpus = args[0];

			scraper.facebookConnection(args[1], args[2]);

//			 lecture du fichier text contenant les info des pages
			String fileContent = FileUtils.readFileToString(new File("../web-scraping-cnrs-lisst/input/pagesInfo.txt"),
					"UTF-8");
			List<String> fileArray = Arrays.asList(fileContent.split("\r\n"));

//			Printer pour le fichier de résultat
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream("../web-scraping-cnrs-lisst/output/" + corpus + "/Facebook/PagesAimees.csv"),
					"UTF-8"));
//			Itération sur toutes les pages du fichier pagesInfo.txt
			for (String info : fileArray) {

				// bench
				long startTime = System.nanoTime();

				String[] pageInfo = info.split(",");
				String pageName = pageInfo[0];

//				recuperation de la page
				System.out.println(pageName);

				getPageAndTOChek(scraper.getDriver(), "https://www.facebook.com/browse/fanned_pages/?id=" + pageInfo[1],
						writer);

//				appuie sur la touche "fin"
				Actions kpress = new Actions(scraper.getDriver());
				int i = 0;
				while (i != 5) {
					kpress.sendKeys(Keys.END).perform();
					Thread.sleep(4 * 1000);
					i++;
				}

//			 	xpath prenant: les pages aimées par la page
				List<String> fanned_pages = new ArrayList<>();
				scraper.getDriver().findElements(By.xpath("//div[@class='fsl fwb fcb']/a"))
						.forEach(fp -> fanned_pages.add(fp.getText()));

//				Erreur si la page courrante n'existe pas
				if (scraper.getDriver().findElements(By.xpath(
						"//div[contains(@class, 'fbProfileBrowserNullstate') and (contains(., 'réessayer') or contains(., 'try again'))]"))
						.size() >= 1) {
					try {
						throw new NoElementFoundException("La page: " + pageName
								+ " n'est pas associée au bon id; vérifier les informations suivantes: " + info);
					} catch (NoElementFoundException e) {
						e.printStackTrace();
						continue;
					}
				}

//				suppression des doublons créé par Facebook
				List<String> fanned_pages_dedupe = fanned_pages.stream().distinct().collect(Collectors.toList());

//				écriture sur le fichier d'une ligne
				String finalCSV;
				for (String string : fanned_pages_dedupe) {
					finalCSV = pageName + "," + string.replace(",", "") + "\n";
					writer.write(finalCSV.toString());
				}

				fanned_pages.clear();
				fanned_pages_dedupe.clear();
				double endTime = System.nanoTime();
				System.out.println("\t--Traitement terminé pour " + pageName + " "
						+ Math.floor((endTime - startTime) / 1000000000.0) + " sec");

//				tempo de 1 min
				scraper.getDriver().get("about:blank");
				Thread.sleep(10 * 1000);

			}
		} catch (InterruptedException | IOException | WebScrapError e) {
			e.printStackTrace();
		} finally {
			scraper.getDriver().quit();
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	private static void getPageAndTOChek(WebDriver driver, String pageURL, BufferedWriter writer)
			throws WebScrapError, IOException {

//		recuperation de la page
		driver.get(pageURL);
//		Dans le cas d'un timed out
		if (!driver.findElements(By.xpath(
				"//div[@id='error-information-popup-content']/div[contains(text(), 'ERR_CONNECTION_TIMED_OUT')]"))
				.isEmpty()) {
			writer.close();
			throw new WebScrapError("Impossible de se connecter a Facebook : Timed_out");
		}
	}
}