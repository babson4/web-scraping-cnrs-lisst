package facebook;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ContentPageScraper {

	/**
	 * @since 2019-05-20
	 * @author Bastien Sibout contact:<a> bastiensib at hotmail.fr </a>
	 * @param corpus: nom du corpus traiter (corespond au nom de dossier de sortie)
	 * @param id:     identifiant de connexion facebook
	 * @param pass:   mot de passe du compte facebook
	 * @throws WebScrapError
	 */

	public static void main(String[] args) throws WebScrapError {
		if (args.length != 3)
			throw new WebScrapError(
					"Nombre d'argument non valide\n Merci de spécifier en arguments: NomDuCorpus IdentifiantCompteFacebook MotDePassFacebook");

		SeleniumScraper scraper = new SeleniumScraper();

//		creation des repertoire
		scraper.createDirectories(args[0]);

		JavascriptExecutor js = (JavascriptExecutor) scraper.getDriver();

//		Printer pour le fichier de résultat
		BufferedWriter writerCsv;
		BufferedWriter writerTxt;

//		liste du résultat
		List<String> scrapedResults = new ArrayList<>();
		List<WebElement> trash = new ArrayList<>();

		try {
//			fichier de résultat
			String corpus = args[0];

			scraper.facebookConnection(args[1], args[2]);

//			 lecture du fichier text contenant les info des pages
			String fileContent = FileUtils.readFileToString(new File("../web-scraping-cnrs-lisst/input/pagesInfo.txt"),
					"UTF-8");
			List<String> fileArray = Arrays.asList(fileContent.split("\r\n"));

//			Itération sur toutes les pages du fichier pagesInfo.txt
			for (String info : fileArray) {
				long startTime = System.nanoTime();

				String[] pageInfo = info.split(",");
				String pageName = pageInfo[0];
				String pageID = pageInfo[1];

				System.out.println("traitement de " + pageName);

//				Printer pour le fichier de résultat CSV
				writerCsv = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(
								"../web-scraping-cnrs-lisst/output/" + corpus + "/Facebook/" + pageName + "_Post.csv"),
						"UTF-8"));

//				Printer pour le fichier de résultat Txt
				writerTxt = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(
								"../web-scraping-cnrs-lisst/output/" + corpus + "/Facebook/" + pageName + "_Post.txt"),
						"UTF-8"));

//				recuperation de la page "d'accueil"
				getPageAndTOChek(scraper.getDriver(), "https://www.facebook.com/" + pageID, writerCsv, writerTxt);

//				Conversion en Date pour comparaison
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

//				recuperation de la page pour les posts
//				check si la page est un groupe fermé
				if (scraper.getDriver().getCurrentUrl().toString().contains("/groups/")) {
					getPageAndTOChek(scraper.getDriver(), "https://www.facebook.com/" + pageID, writerCsv, writerTxt);
				} else {
					getPageAndTOChek(scraper.getDriver(), "https://www.facebook.com/" + pageID + "/posts/", writerCsv,
							writerTxt);
				}

//				appuie sur la touche "fin"
				Actions kpress = new Actions(scraper.getDriver());
				int sameDateCpt = 0;
				Date oldActual = null;
				while (sameDateCpt != 20) {
					List<WebElement> postDates = scraper.getDriver().findElements(By.xpath(
							"//div[@class='_6a _5u5j']/div[@class='_6a _5u5j _6b']/div[@class='_5pcp _5lel _2jyu _232_']//abbr"));
					Date actual = format.parse(postDates.get(postDates.size() - 1).getAttribute("title").split(" ")[0]);

//					tempo pour laisser le temps a la page de chargé (de plus en plus long plus la page est grande)
					kpress.sendKeys(Keys.END).perform();
					System.out.println("scroll \t" + actual);

//					tempo en fonction de la vitesse de chargement
					Thread.sleep(5 * 1000);

//					si la date est toujours la même
					if (actual.equals(oldActual)) {
						sameDateCpt++;
						System.out.println("\t--Meme date " + sameDateCpt + " fois");
//						si la date est la même 10 fois de suite
						if (sameDateCpt > 9) {
//							on remonte legerement la page
							kpress.sendKeys(Keys.PAGE_UP).perform();

							System.out.println("\t--Tempo forcée: " + sameDateCpt + " secondes");
							Thread.sleep(sameDateCpt * 1000);
//							on redescend
							kpress.sendKeys(Keys.END).perform();
							Thread.sleep(1 * 1000);
							kpress.sendKeys(Keys.END).perform();
						}

					} else {
//						si elle change, on remet le compteur à 0
						sameDateCpt = 0;

//						supression des class text_exposed_show pour avoir tout le contenue
						js.executeScript(
								"var elems = document.querySelectorAll('.text_exposed_show'); [].forEach.call(elems, function(el) { el.classList.remove('text_exposed_show'); });");
					}

					oldActual = actual;

					Thread.sleep(1);
				}

//				 sécurité
				kpress.sendKeys(Keys.END).perform();

				System.out.println("JS verif");
				js.executeScript(
						"var elems = document.querySelectorAll('.text_exposed_show'); [].forEach.call(elems, function(el) { el.classList.remove('text_exposed_show'); });");

				System.out.println("get Post");
//				xpath prenant: contenu textuel du post | date
				List<WebElement> post = scraper.getDriver().findElements(By.xpath(
						"//div[@class='_5pbx userContent _3576' or @class='_5pbx userContent _3ds9 _3576'] | //div[@class='_6a _5u5j']/div[@class='_6a _5u5j _6b']/div[@class='_5pcp _5lel _2jyu _232_']//abbr"));

				// Erreur si aucun post trouvé
				if (post.isEmpty()) {
					try {
						throw new NoElementFoundException(
								"Aucun post n'a ete trouve dans la page : " + pageName + ", verifier la ligne " + info);
					} catch (NoElementFoundException e) {
						e.printStackTrace();
						continue;
					}
				}

				System.out.println("trash " + post.size());
//				supprime les dates des post sans contenu textuel
//				ajout des element a supprimer dans trash
				for (int j = 0; j < post.size() - 1; j++)
					if (post.get(j).getTagName().equals(post.get(j + 1).getTagName())) {
						System.out.println("add");
						trash.add(post.get(j));
					}

				System.out.println("delete trash");
//				supprime les elements de post
				for (WebElement webElement : trash) {
					post.remove(webElement);
				}

//				resoud les probleme éventuel de récuperation: post ou date orphelin
				if (post.size() % 2 != 0) {
					// retire le dernier element
					post.remove(post.size() - 1);
				}
				System.out.println("trash end");

//				 On concatene le contenu de l'article et la date
				System.out.println("prep post");
				for (int i = 0; i < post.size() - 2; i += 2) {
					String dateP = post.get(i).getAttribute("title").split(" ")[0]; // supprime l'heure
//					netoyage des caractères qui posent problème
					String contentP = post.get(i + 1).getText().replace("\n", "").replace("\r", "").replace(",", " ")
							.replace("*", "").replace("_", "").replace(":", "").replace("Afficher la suite", " ")
							.replace("la suite", " ");

					scrapedResults.add(dateP + ":" + contentP);
				}
				System.out.println("end prep ... writing ...");
//				écriture sur le fichier CSV d'une ligne
				String CSV = pageName + "," + String.join(",", scrapedResults) + "\n".replace(":", " ");
				writerCsv.write(CSV.toString());
				writerCsv.close();

//				écriture sur le fichier txt d'une ligne
				String txt;
				String date;
				String content;
				for (int i = 0; i < scrapedResults.size(); i++) {
					// variable du jour et du contenue du post
					date = scrapedResults.get(i).split(":")[0];

//					OutOfBound levé pour aucune raison --> force le contenue en cas de probleme
					if (scrapedResults.get(i).split(":").length != 2) {
						content = ".";
					} else {
						content = scrapedResults.get(i).split(":")[1];
					}

					// calcul du numéro de semaine
					Date d = format.parse(date);
					Calendar cal = Calendar.getInstance();
					cal.setTime(d);
					String weekNumYear = cal.get(Calendar.WEEK_OF_YEAR) + date.substring(5);

					String mounthYear = date.substring(3);

//					format Iramuteq

					txt = "**** *name_" + pageName + " *dmy_" + date + " *my_" + mounthYear + " *weeknumy_"
							+ weekNumYear + " \r\n" + content + "\r\n\r\n";

					writerTxt.write(txt.toString());

//					affichage console du résultat
//					System.out.println(date + "\t" + content);
				}
				writerTxt.close();

				double endTime = System.nanoTime();
				System.out.println("Traitement terminé " + Math.floor((endTime - startTime) / 1000000000.0) + " sec"
						+ " nb post: " + scrapedResults.size());

//				ligne suivante: on vide les listes puis on reboucle
				scrapedResults.clear();
				post.clear();
				trash.clear();

//				libère la RAM
				scraper.getDriver().get("https://www.facebook.com/");

//				tempo de 5 min
				System.out.println("idle...");
				Thread.sleep(5 * 60 * 1000);

			}
		} catch (IOException | WebScrapError | InterruptedException | ParseException e) {
			e.printStackTrace();
			scraper.getDriver().quit();
		}
	}

	private static void getPageAndTOChek(WebDriver driver, String pageURL, BufferedWriter writerCsv,
			BufferedWriter writerTxt) throws WebScrapError, IOException {

//		recuperation de la page
		driver.get(pageURL);
//		Dans le cas d'un timed out
		if (!driver.findElements(By.xpath(
				"//div[@id='error-information-popup-content']/div[contains(text(), 'ERR_CONNECTION_TIMED_OUT')]"))
				.isEmpty()) {
			writerCsv.close();
			writerTxt.close();
			throw new WebScrapError("Impossible de se connecter à Facebook : Timed_out");
		}
	}
}