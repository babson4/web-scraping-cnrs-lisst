#!/usr/bin/python
# -*-coding:utf-8 -*

# Script permetant de récuperer des description de compte Twitter via API twitter à partir de leur ID et Pseudo
# et vérifie les tweets collecté 
# Input : corpus
# output: fichier *_final.csv
# usage: python APIGetBiography.py Corpus_name

from sys import argv, stderr
from twython import Twython
import time
import csv
import pandas
import ntpath
import re

def twythonAuth(auth):
	keys = auth.readline().split(',')
	if len(keys) > 1:
		APP_KEY      = str(keys[0].replace('\r', '').replace('\n', '')) 
		APP_SECRET   = str(keys[1].replace('\r', '').replace('\n', '')) 
		twitter      = Twython(APP_KEY, APP_SECRET, oauth_version=2)
		ACCESS_TOKEN = twitter.obtain_access_token()
		twitter      = Twython(APP_KEY, access_token=ACCESS_TOKEN)

		# nombre restant d'appel sur la clé
		remain_call = twitter.get_application_rate_limit_status()["resources"]["users"]["/users/show/:id"]["remaining"]
		print("\n\n{}\t{},{}\n\n".format(remain_call, keys[0], keys[1]))

		return twitter, remain_call 
	else:
		# retour au debut du fichier
		auth.seek(0)
		print("idle 15min ...")
		#attente de 15 min
		time.sleep(60 * 15)
		twythonAuth(auth)

if __name__ == "__main__":
	if(len(argv) > 1):
		with open("../../input/auth.txt", 'r') as auth:
			# API auth + nombre restant d'appel sur la 1ere clé
			twitter, remain_call = twythonAuth(auth)
			with open("../../input/keyword.txt", 'r') as keywords_file:
				keyword_line = keywords_file.readline().replace("\r", "").replace("\n", "")
				while(len(keyword_line) > 0):
					try:
						#lecture du fichier CSV
						data   = pandas.read_csv("../../output/{}/Twitter/{}_combined.csv".format(argv[1], keyword_line), error_bad_lines=False)
						time.sleep(2)
						# supprime les doublons
						data   = data.drop_duplicates()
						
						ids    = data.user_id
						names  = data.username
						tweets = data.tweet

						# colonne pour identifier le ficher avec iramuteq
						data['keyword'] = keyword_line.replace('#', '')
						keyword = data.keyword

						# Validation des tweets collecté
						id_to_remove=[]
						for i in range(len(data)):
							try:
								# Verifie si le mot-clé n'est pas dans le tweet
								if not re.search(str(keyword[i]), str(tweets[i]), re.IGNORECASE):
									id_to_remove.append(i)
							except KeyError:
								#Id supprimer avec le dedoublage des donnees
								continue

						# supprime les lignes non voulu
						data = data.drop(id_to_remove, axis=0)
					

						descs = []
						for i in range(len(data)):
						# 	try:
						# 		if remain_call <= 5: # weird comportement
						# 			print("Switch api key")
						# 			twitter, remain_call = twythonAuth(auth)
								
						# 		#API call
						# 		desc = twitter.show_user(user_id = ids[i], screen_name = names[i])["description"]
						# 		remain_call -= 1
						# 	except Exception:
						# 		#user not found / ban
								descs.append("")
						# 		continue
						# 	#affichage console
						# 	print("{}\t{}\t{}".format(ids[i], names[i], desc.replace('\r', '').replace('\n', ' ') ))
						# 	# supression des retour a la ligne dans les description
						# 	descs.append(desc.replace('\r', '').replace('\n', ' '))

						print("add bios ...")
						#ajout de colonnes au csv avec la description et localisation
						data['bio'] = descs
								
						#ecriture du fichier CSV final
						data.to_csv("../../output/{}/Twitter/{}_final.csv".format(argv[1], keyword_line.replace("\n", '')), index=False, encoding='utf-8')
						time.sleep(2)

						keyword_line = keywords_file.readline()
					except FileNotFoundError:
						print(keyword_line + " not found")
						keyword_line = keywords_file.readline()
						continue
	else:
		stderr.write("Mauvais arguments\n")