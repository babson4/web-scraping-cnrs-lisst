#!/bin/bash
# usage: ./twintScraper CorpusName

while IFS=' ' read -r keyword
do
	twint -s "$keyword" -o "../../output/"+ $1 +"/Twitter/" + keyword + ".csv" --since "2010-01-01" --until "$(date +"%Y-%m-%d")" --csv --location
done < "../../input/keyword"