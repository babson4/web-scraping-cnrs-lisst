#!/usr/bin/python
# -*-coding:utf-8 -*

# Script permetant de crée un corpus au format Iramuteq
# Input : fichier (bio.txt du script "APIGetBiography.py") corpus
# output: fichier corpus.csv

import re
from sys import argv, stderr
import csv
import pandas
import datetime

def checkIfEmpty(string):
	if string == "" or string == " ":
		return "NaN"
	else:
		return string

if __name__ == "__main__":
	if(len(argv) > 2):
		#lecture du fichier CSV
		data = pandas.read_csv(argv[1], error_bad_lines=False)

		# supprime les doublons
		data = data.drop_duplicates()

		tweet_ids = data.id
		dates = data.date
		tweet = data.tweet
		bios = data.bio
		keyword = data.keyword
		locs = data.location

		# ecriture
		with open("../../output/" + argv[2] + "/Twitter/corpusIramuteq.txt", 'w', encoding='utf-8') as corpus:
			for i in range(len(data)):
				dateArray = dates[i].split("-")
				weeknum = datetime.date(int(dateArray[0]), int(dateArray[1].lstrip('0')), int(dateArray[2].lstrip('0'))).isocalendar()[1]
				#mise en forme iramuteq
				corpus.write("**** *tweetId_{} *keyword_{} *dmy_{} *my_{} *year_{} *weeknumy_{}-{} *bio_{} *loc_{} \r\n{}\r\n\r\n".format(checkIfEmpty(tweet_ids[i]), checkIfEmpty(keyword[i]), checkIfEmpty(dates[i]), checkIfEmpty(dates[i][:-3]),checkIfEmpty(dateArray[0]), checkIfEmpty(dateArray[0]), checkIfEmpty(weeknum), checkIfEmpty((str(bios[i]).replace('*', '')),checkIfEmpty(locs[i].replace('*', '')), checkIfEmpty(tweet[i].replace('*', ''))))
	else:
		stderr.write("Mauvais arguments: python createIramuteqCorpus (fichier bio.txt) (nom du corpus)\n")