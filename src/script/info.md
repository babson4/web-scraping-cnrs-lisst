#Script docs

##Date_switch

>Python

Permet de passer les dates du format francais jj/MM/yyyy au format américain yyyy-MM-jj

Argument: nom_corpus_input [nom_corpus_output]

##Extract_URL

>Python

Extrait les URL et le nom d'une page dans un corpus Iramuteq

Argument: nom_corpus_input [nom_fichie_output]

Sortie: fichier.csv au format nom,url

>Besoin d'un traitement excel -- regex a modifier -- 

* Ouvrir le fichier de sortie dans excel
* Convertir le CSV sur les ","
* Dans la colonne A (avec les nom), etendre le nom des pages pour ne pas avoir de cellules vide (possible a faire avec un script VBA)
* Selectionner la colonne B et tout remonter d'une cellule
* selectionner toutes les cellules vides (via la recherche avancer)
* supprimer les lignes des cellules vides
* selectionner la colonne B
* Convertir sur "/" 
* supprimer toutes les autre colonnes (ne garder que B et A)

##Remove_hyttp

>Python

Script permettant de supprimer les url dans un corpus iramuteq

Arguments : nom_corpus_input [nom_corpus_output]

##Concat

>Batch

Script permettant de concatener les fichier .csv et .txt ayant comme nom \*\_post.csv/.txt
dans un fichier end.txt et end.csv

##Extract_date

>Python

Extrait les date dans un corpus Iramuteq 

Arguments : nom_corpus_input [nom_out]

##graph volume de post

>Excel

Dans la colonne A mettre les dates.
Dans la cellule B1 mettre =NB.SI([plage des dates];A1)  (ou si excel en anglais =COUNTIF([plage des dates];A1) ) (ne pas oublier de vérouiller la plage de valeur avec des $)  puis étendre à toute la colonne B.
Selectionner toute la colone B.
copier puis clic droit collage spécial; coller les valeur (icône avec 123).
Puis tout selectionner (commande A) menu donnée puis dans outils de données: supprimer les doublons (prendre les deux collonnes et ok)