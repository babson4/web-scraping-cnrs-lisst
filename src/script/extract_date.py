#!/usr/bin/python
# -*-coding:utf-8 -*

# Script permettant d'extraire les dates corpus iramuteq
# Arguments : nom_corpus_input [nom_output]
# Attention à ne pas mettre le même nom en sortie et en entrée
from sys import argv, stderr, stdout
import re

if __name__ == '__main__':
	if(len(argv) > 1):
		try:
			file = open(argv[1], 'r', encoding='utf-8')
		except:
			stderr.write("Impossible d'ouvrir le fichier\n")
			exit()
		output_name = "date_{}.csv".format(argv[1])
		if(len(argv) > 2):
			output_name = argv[2]
		try:
			sortie = open(output_name, 'w', encoding='utf-8')
		except:
			stderr.write("Impossible de créer le fichier de sortie\n")
			exit()
		line = file.readline()
		stdout.flush()
		sortie.write("dates\n")
		while(len(line) > 0):
			regex = re.compile("\*my_(\d*-\d*)")
			for elem in regex.findall(line):
				sortie.write(elem + "\n")
			line = file.readline()
		sortie.close()
		file.close()
	else:
		stderr.write("Mauvais arguments\n")	