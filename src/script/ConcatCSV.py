#!/usr/bin/python
# -*-coding:utf-8 -*

# Script permettant de concatener des fichier CSV avec entete
# Arguments : patern-to-match

from sys import argv, stderr
import os
import glob
import pandas

if __name__ == "__main__":
	if(len(argv) > 1):
		extension = "csv"
		all_filenames = [i for i in glob.glob('*{}.{}'.format(argv[1], extension))]
		combined_csv = pandas.concat([pandas.read_csv(f) for f in all_filenames ])
		#export to csv
		combined_csv.to_csv("{}_combined.csv".format(argv[1]), index=False, encoding='utf-8')
	else:
		stderr.write("Mauvais arguments\n")