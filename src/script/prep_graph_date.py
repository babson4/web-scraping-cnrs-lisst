# liste = [2,65,42,53,27,2,42,27,2,53,53,53,65,21,27,53,2,53,65,27]
# compte = {}.fromkeys(set(liste),0)
# for valeur in liste:
# 	compte[valeur] += 1

# print(compte)

#!/usr/bin/python
# -*-coding:utf-8 -*


from sys import argv, stderr
import pandas

if __name__ == "__main__":
	if(len(argv) > 1):
		#lecture du fichier CSV
		data = pandas.read_csv(argv[1])

		dates = data.dates

		# # supprime les doublons
		# data = data.drop_duplicates()

		# ecriture
		with open("graph_date_{}.csv".format(argv[1].replace(".txt", '').replace(".csv", "").replace("_clean", "")), 'w', encoding='utf-8') as sortie:
			# for i in range(len(data)):
			res = {}.fromkeys(set(dates),0)
			for valeur in dates:
				res[valeur] += 1
			print(res)
			for k, v in res.items():
				sortie.write("{},{}\n".format(k,v))
	else:
		stderr.write("mauvais arguments")