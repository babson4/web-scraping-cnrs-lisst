#!/usr/bin/python
# -*-coding:utf-8 -*

# Script permettant de remplacer une date jj/MM/yyyy en yyyy-MM-jj dans les variable iramuteq
# Arguments : nom_corpus_input [nom_corpus_output]
# Attention à ne pas mettre le même nom en sortie et en entrée

from sys import argv, stderr, stdout
import re


def replace(obj):
    return '*dmy_' + obj.group(3) + '-' + obj.group(2) + '-' + obj.group(1) + ' *my_' + obj.group(3) + '-' + obj.group(2) + ' *weeknumy_' + obj.group(3) + '-' + obj.group(4)


if __name__ == "__main__":
    if(len(argv) > 1):
        try:
            file = open(argv[1], 'r', encoding='utf-8')
        except:
            stderr.write("Impossible d'ouvrir le fichier\n")
            exit()
        output_name = "date_switch_out.txt"
        try:
            sortie = open(output_name, 'w', encoding='utf-8')
        except:
            stderr.write("Impossible de créer le fichier de sortie\n")
            exit()
        line = file.readline()
        stdout.flush()
        while(len(line) > 0):
            line = re.sub(
                "\*dmy_(\d{2})/(\d{2})/(\d{4})\s\*my_\d{2}/\d{4}\s\*weeknumy_(\d{1,2})/\d{4}\s?", replace, line)
            sortie.write(line)
            line = file.readline()
        sortie.close()
        file.close()
    else:
        stderr.write("Mauvais arguments\n")
